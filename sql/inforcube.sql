create table balance
(
	id int not null
		primary key,
	card_id int not null,
	balance int null
)
;

create index balance_card_id_index
	on balance (card_id)
;

create table cards
(
	id int not null
		primary key,
	bin int not null,
	suffix bigint not null,
	date_making date not null,
	date_expire date not null,
	status int not null,
	constraint cards_balance_fk
		foreign key (id) references balance (card_id)
			on update cascade on delete cascade
)
;

create index cards_status_id_fk
	on cards (status)
;

create index cards_all_index
	on cards (bin, suffix, date_making, date_expire, status)
;

create table orders
(
	id int auto_increment
		primary key,
	card_id int null,
	order_date date null,
	total float null,
	`describe` varchar(100) null
)
;

create index orders_card_id_index
	on orders (card_id)
;

create table status
(
	id int auto_increment
		primary key,
	`describe` varchar(100) not null
)
;

alter table cards
	add constraint cards_status_id_fk
		foreign key (status) references status (id)
			on update cascade
;

create procedure delete_card_by_id (IN card_id int)
BEGIN
  START TRANSACTION;
  DELETE from cards WHERE cards.id = card_id;
  DELETE FROM balance WHERE balance.card_id = card_id;
  DELETE FROM orders WHERE orders.card_id = card_id;
  COMMIT;
END;

create function get_card_balance_by_id (card_id int) returns int
BEGIN
  SELECT balance.balance INTO @balance FROM balance WHERE balance.card_id = card_id;
  RETURN @balance;
END;

create procedure get_card_list ()
BEGIN
  SELECT card.id, card.bin AS `serial`, card.suffix as `number`,
    (CAST(card.date_making AS CHAR(12))) as `date`, (CAST(card.date_expire AS CHAR(12))) AS `expire`, status.`describe` AS `status` FROM cards AS card
    LEFT JOIN status ON card.status = status.id;
END;

create procedure get_card_list_limit_in (IN start_list int, IN end_list int)
BEGIN
  SELECT card.id, card.bin AS `serial`, card.suffix as `number`,
        card.date_making as `date`, card.date_expire AS `expire`, status.`describe` AS `status` FROM cards AS card
    LEFT JOIN status ON card.status = status.id LIMIT start_list, end_list;
END;

create procedure get_orders_by_id_in (IN card_id int)
BEGIN
  SELECT id, order_date, total, `describe` FROM orders WHERE card_id = card_id;
END;

create procedure get_status_list ()
BEGIN
  SELECT * FROM status;
END;

create procedure processing_order_in (IN card_id int, IN order_total int, IN order_describe varchar(100))
BEGIN
  START TRANSACTION;
  SELECT balance.balance INTO @card_db_balance FROM balance WHERE balance.card_id = card_id FOR UPDATE;
  SELECT cards.status INTO @card_db_status FROM cards WHERE cards.id = card_id;

  IF (@card_db_status <> 2)THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Данная карта не активна, списание не возможно';
    ROLLBACK;
  END IF;

  IF ((@card_db_balance - order_total)<0)THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Не достаточно средств на балансе карты.';
    ROLLBACK;
  END IF;

  INSERT INTO orders(card_id, order_date, total, `describe`) VALUES (card_id,CURRENT_DATE,order_total,order_describe);
  UPDATE balance SET balance.balance = (@card_db_balance - order_total) WHERE balance.card_id = card_id;

  COMMIT;
END;

create procedure register_new_card_in (IN bin int, IN suffix bigint, IN date_making varchar(15), IN date_expire varchar(15), IN balance float)
BEGIN
  START TRANSACTION;
  SELECT MAX(id)+1 INTO @balance_id FROM balance;
  SELECT MAX(id)+1 INTO @card_id FROM cards;
  IF (@balance_id is NULL ) THEN
    SET @balance_id = 1;
  END IF;
  IF (@card_id is NULL ) THEN
    SET @card_id = 1;
  END IF;
  INSERT INTO balance(id, card_id, balance) VALUES (@balance_id, @card_id, balance);
  INSERT INTO cards(id, bin, suffix, date_making, date_expire, status)
          VALUES(@card_id, bin, suffix, str_to_date(date_making,"%d-%m-%Y" ), str_to_date(date_expire,"%d-%m-%Y"), 1);
  COMMIT;
END;

create procedure search_card_info_in (IN card_id int, IN card_bin int, IN card_suffix bigint, IN card_date_making varchar(15), IN card_date_expire varchar(15), IN card_status int, IN Debug tinyint(1))
BEGIN
  DECLARE second_param BOOLEAN;
  DECLARE query_string TEXT;
  IF (Debug)
    THEN
    SET @query_string = 'EXPLAIN SELECT card.id as `id`, card.bin as `serial`, card.suffix as `number`,
                          CAST(card.date_making AS CHAR(12)) as `date`,
                          CAST(card.date_expire AS CHAR(12)) as `expire`,
                          status.describe as `status`FROM cards AS card
                          LEFT JOIN status ON card.status = status.id';
    ELSE
    SET @query_string = 'SELECT card.id as `id`, card.bin as `serial`, card.suffix as `number`,
                          CAST(card.date_making AS CHAR(12)) as `date`,
                          CAST(card.date_expire AS CHAR(12)) as `expire`,
                          status.describe as `status`FROM cards AS card
                          LEFT JOIN status ON card.status = status.id';
  END IF;

  SET second_param = FALSE;

  # Проверка на поиск по id
  If (card_id is NOT NULL)
    THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.id=',card_id);
      END;
  END IF;

  #Проверка на поиск по БИН
  IF ((card_bin is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.bin=',card_bin);
    END;
    ELSE IF (card_bin IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.bin=',card_bin);
      END;
    END IF;
  END IF;

  #Проверка на поиск по суффиксу
  IF ((card_suffix is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.suffix=',card_suffix);
    END;
    ELSE IF (card_suffix IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.suffix=',card_suffix);
      END;
    END IF;
  END IF;

  #Проверка на поиск по дате изготовления
  IF ((card_date_making is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.date_making="',str_to_date(card_date_making,"%d-%m-%Y"),'"');
    END;
    ELSE IF (card_date_making IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.date_making="',str_to_date(card_date_making,"%d-%m-%Y"),'"');
      END;
    END IF;
  END IF;

  #Проверка на поиск по дате окончания действия
  IF ((card_date_expire is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.date_expire="',str_to_date(card_date_expire,"%d-%m-%Y"),'"');
    END;
    ELSE IF (card_date_expire IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.date_expire="',str_to_date(card_date_expire,"%d-%m-%Y"),'"');
      END;
    END IF;
  END IF;

  #Проверка на поиск по статусу
  IF ((card_status is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND status.id=',card_status);
    END;
    ELSE IF (card_status IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE status.id=',card_status);
      END;
    END IF;
  END IF;

  IF (Debug) THEN
    SELECT @query_string;
  END IF;

  PREPARE getSearch FROM @query_string;
  EXECUTE getSearch;
  DEALLOCATE PREPARE getSearch;

END;

create procedure update_card_status_in (IN card_id int, IN card_status int)
BEGIN
  START TRANSACTION;
    UPDATE cards SET status=card_status WHERE id = card_id;
  COMMIT;
END;

INSERT INTO inforcube.status (`describe`) VALUES ('Не активна');
INSERT INTO inforcube.status (`describe`) VALUES ('Активна');
INSERT INTO inforcube.status (`describe`) VALUES ('Просрочена');
INSERT INTO inforcube.status (`describe`) VALUES ('Заблокирована');