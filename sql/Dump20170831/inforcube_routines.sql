-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: 192.168.100.110    Database: inforcube
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'inforcube'
--
/*!50003 DROP FUNCTION IF EXISTS `get_card_balance_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` FUNCTION `get_card_balance_by_id`(card_id INT) RETURNS int(11)
BEGIN
  SELECT balance.balance INTO @balance FROM balance WHERE balance.card_id = card_id;
  RETURN @balance;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_card_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `delete_card_by_id`(IN card_id INT)
BEGIN
  START TRANSACTION;
  DELETE from cards WHERE cards.id = card_id;
  DELETE FROM balance WHERE balance.card_id = card_id;
  DELETE FROM orders WHERE orders.card_id = card_id;
  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_card_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `get_card_list`()
BEGIN 
  SELECT card.id, card.bin AS `serial`, card.suffix as `number`,
    (CAST(card.date_making AS CHAR(12))) as `date`, (CAST(card.date_expire AS CHAR(12))) AS `expire`, status.`describe` AS `status` FROM cards AS card
    LEFT JOIN status ON card.status = status.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_card_list_limit_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `get_card_list_limit_in`(IN start_list INT, IN end_list INT)
BEGIN 
  SELECT card.id, card.bin AS `serial`, card.suffix as `number`,
        card.date_making as `date`, card.date_expire AS `expire`, status.`describe` AS `status` FROM cards AS card
    LEFT JOIN status ON card.status = status.id LIMIT start_list, end_list;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_orders_by_id_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `get_orders_by_id_in`(IN card_id INT)
BEGIN
  SELECT id, order_date, total, `describe` FROM orders WHERE card_id = card_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_status_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `get_status_list`()
BEGIN
  SELECT * FROM status;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `processing_order_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `processing_order_in`(IN card_id INT, IN order_total INT, IN order_describe VARCHAR(100))
BEGIN
  START TRANSACTION;
  SELECT balance.balance INTO @card_db_balance FROM balance WHERE balance.card_id = card_id FOR UPDATE;
  SELECT cards.status INTO @card_db_status FROM cards WHERE cards.id = card_id;

  IF (@card_db_status <> 2)THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Данная карта не активна, списание не возможно';
    ROLLBACK;
  END IF;

  IF ((@card_db_balance - order_total)<0)THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Не достаточно средств на балансе карты.';
    ROLLBACK;
  END IF;

  INSERT INTO orders(card_id, order_date, total, `describe`) VALUES (card_id,CURRENT_DATE,order_total,order_describe);
  UPDATE balance SET balance.balance = (@card_db_balance - order_total) WHERE balance.card_id = card_id;

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `register_new_card_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `register_new_card_in`(IN bin         INT, IN suffix BIGINT, IN date_making VARCHAR(15),
                                      IN date_expire VARCHAR(15), IN balance FLOAT)
BEGIN
  START TRANSACTION;
  SELECT MAX(id)+1 INTO @balance_id FROM balance;
  SELECT MAX(id)+1 INTO @card_id FROM cards;
  IF (@balance_id is NULL ) THEN
    SET @balance_id = 1;
  END IF;
  IF (@card_id is NULL ) THEN
    SET @card_id = 1;
  END IF;
  INSERT INTO balance(id, card_id, balance) VALUES (@balance_id, @card_id, balance);
  INSERT INTO cards(id, bin, suffix, date_making, date_expire, status)
          VALUES(@card_id, bin, suffix, str_to_date(date_making,"%d-%m-%Y" ), str_to_date(date_expire,"%d-%m-%Y"), 1);
  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_card_info_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `search_card_info_in`(IN card_id          INT, IN card_bin INT, IN card_suffix BIGINT,
                                     IN card_date_making VARCHAR(15), IN card_date_expire VARCHAR(15), IN card_status INT,
                                     IN Debug            TINYINT(1))
BEGIN
  DECLARE second_param BOOLEAN;
  DECLARE query_string TEXT;
  IF (Debug)
    THEN
    SET @query_string = 'EXPLAIN SELECT card.id as `id`, card.bin as `serial`, card.suffix as `number`,
                          CAST(card.date_making AS CHAR(12)) as `date`,
                          CAST(card.date_expire AS CHAR(12)) as `expire`,
                          status.describe as `status`FROM cards AS card
                          LEFT JOIN status ON card.status = status.id';
    ELSE
    SET @query_string = 'SELECT card.id as `id`, card.bin as `serial`, card.suffix as `number`,
                          CAST(card.date_making AS CHAR(12)) as `date`,
                          CAST(card.date_expire AS CHAR(12)) as `expire`,
                          status.describe as `status`FROM cards AS card
                          LEFT JOIN status ON card.status = status.id';
  END IF;

  SET second_param = FALSE;

  # Проверка на поиск по id
  If (card_id is NOT NULL)
    THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.id=',card_id);
      END;
  END IF;

  #Проверка на поиск по БИН
  IF ((card_bin is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.bin=',card_bin);
    END;
    ELSE IF (card_bin IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.bin=',card_bin);
      END;
    END IF;
  END IF;

  #Проверка на поиск по суффиксу
  IF ((card_suffix is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.suffix=',card_suffix);
    END;
    ELSE IF (card_suffix IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.suffix=',card_suffix);
      END;
    END IF;
  END IF;

  #Проверка на поиск по дате изготовления
  IF ((card_date_making is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.date_making="',str_to_date(card_date_making,"%d-%m-%Y"),'"');
    END;
    ELSE IF (card_date_making IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.date_making="',str_to_date(card_date_making,"%d-%m-%Y"),'"');
      END;
    END IF;
  END IF;

  #Проверка на поиск по дате окончания действия
  IF ((card_date_expire is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND card.date_expire="',str_to_date(card_date_expire,"%d-%m-%Y"),'"');
    END;
    ELSE IF (card_date_expire IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE card.date_expire="',str_to_date(card_date_expire,"%d-%m-%Y"),'"');
      END;
    END IF;
  END IF;

  #Проверка на поиск по статусу
  IF ((card_status is NOT NULL) AND second_param)
    THEN
    BEGIN
      SET @query_string = CONCAT(@query_string,' ',' AND status.id=',card_status);
    END;
    ELSE IF (card_status IS NOT NULL )
      THEN
      BEGIN
        SET second_param = TRUE;
        SET @query_string = CONCAT(@query_string,' ','WHERE status.id=',card_status);
      END;
    END IF;
  END IF;

  IF (Debug) THEN
    SELECT @query_string;
  END IF;

  PREPARE getSearch FROM @query_string;
  EXECUTE getSearch;
  DEALLOCATE PREPARE getSearch;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_card_status_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infocube`@`%` PROCEDURE `update_card_status_in`(IN card_id INT, IN card_status INT)
BEGIN
  START TRANSACTION;
    UPDATE cards SET status=card_status WHERE id = card_id;
  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-31  0:12:00
