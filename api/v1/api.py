# -*- coding: UTF-8 -*-
from application import mysql


def sql_get_status_list():
    """
    Возвращает список доступных статусов из БД
    :return: stats: dict содержащий статусы
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('get_status_list')
    stats = cursor.fetchall()
    listing = converting(stats)
    return listing


def sql_get_card_list_limit(params):
    """
    Возвращает список зарешистрированных карт
    :param params: tuple - содержит ограничения для запроса (SQL LIMIT) числа
    :return: listing: dict - список карт
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('get_card_list_in', params)
    cards = cursor.fetchall()
    listing = converting(cards)
    return listing


def sql_get_card_list():
    """
    Возвращает список карт без ограничений на количество
    :return: listing: dict - список карт
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('get_card_list')
    cards = cursor.fetchall()
    listing = converting(cards)
    return listing


def update_card_status_by_id(params):
    """
    Обновляет статус карты
    :param params: список параметров: id -уникальный id карты status - уникальный id статуса
    :return: NONE
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('update_card_status_in', params)


def sql_register_card(params):
    """
    Регистрирует карту в бд.
    :param params: список - содержит параметры для регистрации
    :return: NONE
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('register_new_card_in', params)


def sql_delete_card(card_id):
    """
    Удаляет карту по id
    :param card_id: int id удаляемой карты
    :return: NONE
    """
    params = []
    params.append(card_id)
    cursor = mysql.connect.cursor()
    cursor.callproc('delete_card_by_id', params)


def sql_search_card(params):
    """
    Осуществляет поиск карты по выбранным полям в БД
    :param params: список параметров для поиска
    :return: NONE
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('search_card_info_in', params)
    cards = cursor.fetchall()
    linsting = converting(cards)
    return linsting


def sql_add_order(params):
    """
    Оформляет заказа и проводит списание средств с баланса.
    :param params:
    :return: NONE
    """
    cursor = mysql.connect.cursor()
    cursor.callproc('processing_order_in', params)


def converting(answer):
    x = 0
    resp = dict()
    for i in answer:
        x = x + 1
        resp['object_'+str(x)] = i
    return resp
