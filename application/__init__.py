from flask import Flask
from config import Production, Develop
from flask_debugtoolbar import DebugToolbarExtension
from flask_mysqldb import MySQL
from flask.ext.restful import Api

card_client = Flask(__name__)
card_client.config.from_object(Develop)
mysql = MySQL(card_client)
card_api = Api(card_client)

if card_client.debug:
    toolbar = DebugToolbarExtension(card_client)

from application import views
