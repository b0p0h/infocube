$(document).ready(function () {

    CardList();

    $("#add-card-modal-dialog").hide();

    $("#search-card-modal-dialog").hide();

    $("#order-add-modal-dialog").hide();

    $("#submit-search-form-button").click(ProcessSearchForm);

    $("#submit-add-card-form-button").click(ProcessAddForm);

    $("#submit-order-add-form-button").click(ProcessAddOrderForm);

})

function CardList(){
    // получаем список карт
    $.ajax({
        url: "/api/v1/cardlist",
        type: "GET",
        success: function (result) {
            UpdateCardList(result)
        },

    })
    // прикрепляем
}

function UpdateCardList(result){
    $("#card-list").empty();

    for (object in result){
        temp = "<!-- Шаблон карты -->"
        temp = temp+ '<div class="grid_3 list-of-cards">'+result[object].serial+'</div>'
        temp = temp+ '<div class="grid_3 list-of-cards">'+result[object].number+'</div>'
        temp = temp+ '<div class="grid_3 list-of-cards">'+result[object].date+'</div>'
        temp = temp+ '<div class="grid_3 list-of-cards">'+result[object].expire+'</div>'
        temp = temp+ '<div class="grid_3 list-of-cards omega">'+result[object].status+'</div>'
        temp = temp+ '<div class="clear"></div>'
        temp = temp+ '<div class="grid_16">'
        temp = temp+ '<div id="accordion-top">'
        temp = temp+ '<h3>Подробнее</h3>'
        temp = temp+ '<div style="text-align: center">'
        temp = temp+ '<div id="accordion">'
        temp = temp+ '<h3>Действия с картой</h3>'
        temp = temp+ '<div style="text-align: right">'

        temp = temp+ '<select name="status" id="card_'+result[object].id+'">'
        temp = temp+ '</select>'
        temp = temp+ '<input name="change-card-status" id="'+result[object].id+'" class="ui-button ui-widget ui-corner-all" type="button" value="Сменить статус">'
        temp = temp+ '<input name="card-add-order" id="'+result[object].id+'" class="ui-button ui-widget ui-corner-all" type="button" value="Оформить покупку">'
        temp = temp+ '<input name="delete" class="ui-button ui-widget ui-corner-all" id="'+result[object].id+'" type="button" value="Удалить">'
        temp = temp+ "</div>"
        temp = temp+ "<h3>Покупки по карте</h3>"
        temp = temp+ "<div>"
        temp = temp+ "<p>1234</p>"
        temp = temp+ "</div>"
        temp = temp+ "</div>"
        temp = temp+ "</div>"
        temp = temp+ "</div>"
        temp = temp+ "</div>"
        temp = temp+ '<div class="clear"></div>'

        $("#card-list").append(temp);

    }

    temp = '<div class="grid_16 delimiter"></div><div class="clear"></div>'
    $("#card-list").append(temp)

    UserInterface();
}

function UserInterface(){
    $("div[id=accordion]").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
        activate: function (event, ui) {
            //alert (ui)
        },
    })

    $("div[id=accordion-top]").accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    })

    $("select[name=status]").selectmenu();
    $("select[name=search-card-status]").selectmenu();

    GetSatusList();

    $("#add-card-button-dialog").click(ShowAddCartMenu);

    $("#search-card-button-dialog").click(ShowSearchDialog);

    $("#cancel-add-card-form-button").click(function () {
        $("#add-card-modal-dialog").dialog('close')
    })

    $("#cancel-search-card-form-button").click(function () {
        $("#search-card-modal-dialog").dialog('close')
    });

    $("input[name=delete]").click(DeleteCard);
    $("input[name=change-card-status]").click(ChangeCardStatus);
    $("input[name=card-add-order]").click(AddOrder);
}

function GetSatusList(){
    $.ajax({
        url: '/api/v1/statuslist',
        type: 'GET',
        success: function (result) {
            $("select[name=status]").empty()
            for (object in result){
                temp = '<option value="'+result[object].id+'">'+result[object].describe+'</option>'
                $("select[name=status]").append(temp)
                $("select[name=search-card-status]").append(temp)
            }
        }

    })
}

function ShowAddCartMenu(){

    $("#add-card-modal-dialog").dialog({
        title: "Регистрация новой карты",
        modal: true,
    });

    $("#date-expire").datepicker({
        dateFormat: "dd-mm-yy",
    });
    $("#date-making").datepicker({
        dateFormat: "dd-mm-yy",
    });


}

function ShowSearchDialog() {
    $("#search-card-modal-dialog").dialog({
        title: 'Поиск карты',
        modal: true
    });

    $("#search-card-status").select().val('')

    $("#search-date-expire").datepicker({
        dateFormat:"dd-mm-yy",
    });
    $("#search-date-making").datepicker({
        dateFormat: "dd-mm-yy",
    });
}

function ProcessSearchForm(){
    $("#search-card-modal-dialog").dialog('close');
    $.ajax({
        url: '/api/v1/search',
        type: "POST",
        data: $("#search-card-form").serialize(),
        success: function (result) {
            UpdateCardList(result)
        }
    })
}

function ProcessAddForm() {
    $("#add-card-modal-dialog").dialog('close');
    $.ajax({
        url: '/api/v1/card',
        type: 'POST',
        data: $("#add-card-form").serialize(),
        success: CardList,
    })
}

function DeleteCard() {
    $.ajax({
        url: '/api/v1/card',
        type: 'DELETE',
        data: {"id":this.id},
        success: CardList,
    })
}

function ChangeCardStatus() {
    $.ajax({
        url: '/api/v1/card',
        data: {"id":this.id,"status":$("#card_"+this.id).val()},
        type: 'PUT',
        success: CardList,
    })
}

function AddOrder() {
    $("#order-add-modal-dialog").dialog();
    temp = '<input type="hidden" name="order-card-id" value="'+this.id+'">'
    $("#order-add-form").append(temp)
}

function GetOrderList(ui){
    var active = this.accordion("option", "active")
    alert(active);
}

function ProcessAddOrderForm (){
    $("#order-add-modal-dialog").dialog('close')
    $.ajax({
        url: '/api/v1/orders',
        type: "POST",
        data: $("#order-add-form").serialize(),
        success: function () {
            alert("Успешно")
        }
    })

}