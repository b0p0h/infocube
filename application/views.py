# -*- coding: UTF-8
from flask import render_template, make_response
from application import card_client, card_api
from flask.ext.restful import Resource
from flask_restful import reqparse
from flask import jsonify
from api.v1 import api


@card_client.route('/', methods=['GET'])
def index_card_list():
    return render_template('index.html')


class StatusList(Resource):

    __not_allowed = dict()

    def __init__(self):
        """
        Инициализируем заглушка для не используемых методов
        """
        self.__not_allowed['code'] = '1.1.1'
        self.__not_allowed['message'] = 'Данный запрос не разрешен'
        self.__not_allowed['deatil'] = 'Более подробно по адресу http://wiki/error/1.1.1.html'

    def get(self):
        """
        Получает список статусов карт
        :return: JSON
        """
        return make_response(jsonify(api.sql_get_status_list()), 200)

    def post(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def delete(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def put(self):
        return make_response(jsonify(self.__not_allowed), 400)


class CardList(Resource):

    __not_allowed = dict()

    def __init__(self):
        """
        Инициализируем заглушку для не используемых методов
        """
        self.__not_allowed['code'] = '1.1.1'
        self.__not_allowed['message'] = 'Данный запрос не разрешен'
        self.__not_allowed['deatil'] = 'Более подробно по адресу http://wiki/error/1.1.1.html'

    def get(self):
        """
        Возвращает список зарегистрированных карт
        :return: JSON
        """
        return make_response(jsonify(api.sql_get_card_list()), 200)

    def post(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def put(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def delete(self):
        return make_response(jsonify(self.__not_allowed), 400)


class Card(Resource):

    __not_allowed = dict()

    def __init__(self):
        """
        Инициализируем заглушку для не используемых методов
        """
        self.__not_allowed['code'] = '1.1.1'
        self.__not_allowed['message'] = 'Данный запрос не разрешен'
        self.__not_allowed['deatil'] = 'Более подробно по адресу http://wiki/error/1.1.1.html'

    def get(self):
        pass

    def post(self):
        """
        Принимет значения из формы для дальнейшей регистрации
        :param card-number: int Номер карты 16ть цифр
        :param date-making: str Дата изготовления
        :param date-expire: str Дата окончания действия
        :param balance: int баланс карты
        :return: JSON
        """
        parser = reqparse.RequestParser()
        parser.add_argument('card-number', type=str)
        parser.add_argument('date-making', type=str)
        parser.add_argument('date-expire', type=str)
        parser.add_argument('balance', type=str)
        args = parser.parse_args()
        params = []
        params.append(str_to_int(str(args['card-number'])[:6]))
        params.append(str_to_int(str(args['card-number'])[5:16]))
        params.append(args['date-making'])
        params.append(args['date-expire'])
        params.append(args['balance'])
        api.sql_register_card(params)
        return make_response(jsonify({"status": "OK"}), 200)

    def put(self):
        """
        Обновляет карту с выбранным id присваивая ей id выбранного статуса
        :return: JSON
        """
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=str)
        parser.add_argument('status', type=str)
        params = []
        args = parser.parse_args()
        params.append(str_to_int(args['id']))
        params.append(str_to_int(args['status']))
        api.update_card_status_by_id(params)
        return make_response(jsonify({"status": "OK"}), 200)

    def delete(self):
        """
        Удаляет карту по выбранному id
        :return: JSON
        """
        parser = reqparse.RequestParser();
        parser.add_argument('id', type=str)
        params = parser.parse_args()
        card_id = str_to_int(params['id'])
        api.sql_delete_card(card_id)
        return make_response(jsonify({"status": "OK"}), 200)


class Search(Resource):

    __not_allowed = dict()

    def __init__(self):
        """
        Инициализируем заглушку для неиспользуемых методов
        """
        self.__not_allowed['code'] = '1.1.1'
        self.__not_allowed['message'] = 'Данный запрос не разрешен'
        self.__not_allowed['deatil'] = 'Более подробно по адресу http://wiki/error/1.1.1.html'

    def get(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def post(self):
        """
        Принимет и готовит параметры для поиска значения в БД
        :return: JSON
        """
        parser = reqparse.RequestParser()
        parser.add_argument('search-card-bin', type=str, dest='bin')
        parser.add_argument('search-card-number', type=str, dest='suffix')
        parser.add_argument('search-date-expire', type=str, dest='expire')
        parser.add_argument('search-date-making', type=str, dest='making')
        parser.add_argument('search-card-status', type=str, dest='status')
        parser.add_argument('search-card-id', type=str, dest='id', required=False)
        args = parser.parse_args()
        print args
        args['expire'] = check_str(args['expire'])
        args['making'] = check_str(args['making'])
        params = (str_to_int(args['id']), str_to_int(args['bin']), str_to_int(args['suffix']),
                  args['making'], args['expire'], str_to_int(args['status']), False)
        print params
        listing = api.sql_search_card(params)
        print listing
        return make_response(jsonify(listing), 200)

    def put(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def delete(self):
        return make_response(jsonify(self.__not_allowed), 400)

class Orders(Resource):

    __not_allowed = dict()

    def __init__(self):
        """
        Инициализируем заглушку для неиспользуемых методов
        """
        self.__not_allowed['code'] = '1.1.1'
        self.__not_allowed['message'] = 'Данный запрос не разрешен'
        self.__not_allowed['deatil'] = 'Более подробно по адресу http://wiki/error/1.1.1.html'

    def get(self):
        """
        Получает список заказов для размещения в подробностях о карте
        :return:
        """
        pass

    def post(self):
        """
        Добавляет заказ в список заказов.
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('order-card-id', type=str, dest='id')
        parser.add_argument('order-total', type=str, dest='total')
        parser.add_argument('order-describe', type=unicode, dest='describe')
        args = parser.parse_args()
        params = []
        params.append(str_to_int(args['id']))
        params.append(str_to_int(args['total']))
        params.append(args['describe'])
        api.sql_add_order(params)
        return make_response(jsonify({"status": "OK"}), 200)

    def put(self):
        return make_response(jsonify(self.__not_allowed), 400)

    def delete(self):
        return make_response((jsonify(self.__not_allowed)), 400)


card_api.add_resource(StatusList, '/api/v1/statuslist')
card_api.add_resource(CardList, '/api/v1/cardlist')
card_api.add_resource(Card, '/api/v1/card')
card_api.add_resource(Search, '/api/v1/search')
card_api.add_resource(Orders, '/api/v1/orders')


def str_to_int(val):
    try:
        return int(val)
    except:
        return None


def check_str(val):
    if val == '':
        None
    else:
        return Val